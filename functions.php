<?php //子テーマ用関数

//親skins の取得有無の設定
function include_parent_skins(){
  return true; //親skinsを含める場合はtrue、含めない場合はfalse
}

//子テーマ用のビジュアルエディタースタイルを適用
add_editor_style();

//以下にSimplicity子テーマ用の関数を書く

//
// Simplicity2-child/myPhpFiles の php ファイルを開くためのコード
//
function Include_my_php($params = array()) {
    extract(shortcode_atts(array(
      'file' => 'default',
      'placeid' => 'null'
    ), $params));
    ob_start();
    include(get_theme_root() . "/simplicity2-child/myPhpFiles/$file.php");
    showTweet($placeid);
    return ob_get_clean();
}

add_shortcode('myphp', 'Include_my_php');


//
// Simplicity2-child/myJsFiles の js ファイルを開くためのコード
//
function Include_my_js($params = array()) {
    extract(shortcode_atts(array(
      'file' => 'default',
      'placeid' => 'null'
    ), $params));
    ob_start();
    include(get_theme_root() . "/simplicity2-child/myJsFiles/$file.js");
    showTweet($placeid);
    return ob_get_clean();
}

add_shortcode('myjs', 'Include_my_js');
