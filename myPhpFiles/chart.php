
<?php

function showChart($placeid) {
$url1 = "http://153.126.171.99:3000/pie.json?a=";
$url2 = "&l=";
$pageid = "1";
$konid = "3";
$charturl = "{$url1}{$placeid}{$url2}{$day}";

return $charturl;

};

function showTweetSuiteru($placeid) {
$url1 = "http://153.126.171.99:3000/tweetdata.json?a=";
$url2 = "&page=";
$url3 = "&kon=";
$pageid = "1";
$konid = "1";
$suiteruurl = "{$url1}{$placeid}{$url2}{$pageid}{$url3}{$konid}";

return $suiteruurl;

};

function showTweetKininaru($placeid) {
$url1 = "http://153.126.171.99:3000/tweetdata.json?a=";
$url2 = "&page=";
$url3 = "&kon=";
$pageid = "1";
$konid = "2";
$kininaruurl = "{$url1}{$placeid}{$url2}{$pageid}{$url3}{$konid}";

return $kininaruurl;

};

function showTweetKonderu($placeid) {
$url1 = "http://153.126.171.99:3000/tweetdata.json?a=";
$url2 = "&page=";
$url3 = "&kon=";
$pageid = "1";
$konid = "3";
$konderuurl = "{$url1}{$placeid}{$url2}{$pageid}{$url3}{$konid}";

return $konderuurl;

};
 ?>

<div class ="chart_left">
<canvas id="mycanvas" height=600 width=450 ></canvas>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

//$.getJSON("/wordpress/wp-content/themes/simplicity2-child/myPhpFiles/pie.json", function(data){
$.getJSON("<?php echo showChart($placeid)?>", function(data){
var chartdata = [
 {
  value: data[0],
  color:"#00A0E9",
  highlight: "#FF5A5E",
  label: "空いてる"
 },
 {
  value: data[1],
  color: "#019c96",
  highlight: "#5AD3D1",
  label: "気になる"
 },
 {
  value: data[2],
  color: "#E4007F",
  highlight: "#FFC870",
  label: "混んでる"
}
];

var myChart = new Chart(document.getElementById("mycanvas").getContext("2d")).Doughnut(chartdata,{responsive:true});

});

</script>

</div>

<div class="chart_right">
<br/><br/><div class="legend_konderu">混んでる</div><div class="legend_kininaru">気になる</div><div class="legend_suiteru">空いてる</div>
</div>
<div class="question_Box">
<div class="konzatsu_tweet">混んでるツイート</div><br/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type='text/javascript'>

//ここからgetJSONで配列を取得する
$.getJSON("<?php echo showTweetKonderu($placeid)?>", function(data){
  var arr = data[0]["arrydata1"];
  for (var i in arr) {

  var obj = arr[i];
  var imageUrl = obj.u_profile_image_url;
  var name = obj.u_name;
  var text = obj.text;
  var created_at = obj.created_at;
  var contentHtml = "";

  if (imageUrl)
  {
    contentHtml += '<div class="answer_image"><img src="' + imageUrl + '">';
  }
  if (name)
  {
    contentHtml += '<p class="name">' + name + '</p>' + '</div>';
  }
  if (text)
  {
    contentHtml += '<div class="arrow_answer">' + text + '</div>';
  }
  if (created_at)
  {
    contentHtml += '<div class="created_time">' + created_at + '</div>' + '<br/>';
  }
  document.getElementsByClassName("question_Box")[0].innerHTML += contentHtml;

};


});

</script>
</div>

<div class="question_Box">
<div class="kininaru_tweet">気になるツイート</div><br/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type='text/javascript'>

//ここからgetJSONで配列を取得する
$.getJSON("<?php echo showTweetKininaru($placeid)?>", function(data){
  var arr = data[0]["arrydata1"];
  for (var i in arr) {

  var obj = arr[i];
  var imageUrl = obj.u_profile_image_url;
  var name = obj.u_name;
  var text = obj.text;
  var created_at = obj.created_at;
  var contentHtml = "";

  if (imageUrl)
  {
    contentHtml += '<div class="answer_image"><img src="' + imageUrl + '">';
  }
  if (name)
  {
    contentHtml += '<p class="name">' + name + '</p>' + '</div>';
  }
  if (text)
  {
    contentHtml += '<div class="arrow_answer">' + text + '</div>';
  }
  if (created_at)
  {
    contentHtml += '<div class="created_time">' + created_at + '</div>' + '<br/>';
  }
  document.getElementsByClassName("question_Box")[1].innerHTML += contentHtml;

};


});

</script>
</div>


<div class="question_Box">
<div class="suiteru_tweet">空いてるツイート</div><br/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type='text/javascript'>

//ここからgetJSONで配列を取得する
$.getJSON("<?php echo showTweetSuiteru($placeid)?>", function(data){
  var arr = data[0]["arrydata1"];
  for (var i in arr) {

  var obj = arr[i];
  var imageUrl = obj.u_profile_image_url;
  var name = obj.u_name;
  var text = obj.text;
  var created_at = obj.created_at;
  var contentHtml = "";

  if (imageUrl)
  {
    contentHtml += '<div class="answer_image"><img src="' + imageUrl + '">';
  }
  if (name)
  {
    contentHtml += '<p class="name">' + name + '</p>' + '</div>';
  }
  if (text)
  {
    contentHtml += '<div class="arrow_answer">' + text + '</div>';
  }
  if (created_at)
  {
    contentHtml += '<div class="created_time">' + created_at + '</div>' + '<br/>';
  }
  document.getElementsByClassName("question_Box")[2].innerHTML += contentHtml;

};


});

</script>

</div>
